import Link from "next/link";
import React from "react";
import CopyRightFooter from "./CopyRightFooter";
const logo = "./assets/footerLogoV4.png";
const arrow = "./assets/rightArrow.png";
const placeholder = "./assets/A3.png";

const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="innerContainer">
          <div className="logoRow">
            <Link className="home-link" href="/" passHref>
              <img src={logo} alt="logo" className="logo" />
            </Link>
          </div>
          <div className="row no-gutters">
            <div className="col-sm-12 col-md-12 col-lg-4">
             <div className="d-flex justify-content-center align-items-center h-100">
                 <a href="#" className="btnGetStarted Withdraw">
                  PLAY NOW
                 </a>
             </div>
            </div>
            <div className="col-6 col-md-4 col-lg-3">
              <ul className="navList pl">
                <li className="navLink active">Quick Access</li>
                <li className="navLink">Access Passes</li>
                <li className="navLink">Lore</li>
              </ul>
            </div>
            <div className="col-6 col-md-4 col-lg-3">
              <ul className="navList pl">
                <li className="navLink active"></li>
                <li className="navLink">Dashboard</li>
                <li className="navLink">Updates</li>
              </ul>
            </div>
            <div className="col-6 col-md-4 col-lg-2">
              <ul className="navList">
                <li className="navLink active"></li>
                <li className="navLink">Whitepaper</li>
                <li className="navLink">FAQ’s</li>
              </ul>
            </div>
          </div>
          <CopyRightFooter />
        </div>
      </div>
    </div>
  );
};

export default Footer;
