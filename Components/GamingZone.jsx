import React from "react";
import { card1, card2, card3, card4, card5, card6 } from "../styles/utilsStyle";
import GamingZoneItem from "./GamingZoneItem";
const mintingGame = "./assets/mintingTextInner.png";

const GamingZone = () => {
  return (
    <div className="GamingZonePlay abOne">
      <div className="container">
        
        <div className="GamingZoneItemRow">
          <div className="headingWrapper">
              <h1 className="heading-img"><img src={mintingGame} className="heading text-center"/></h1>
              <p className="dec-minting">
              Choose your Access Pass and enter The Empire Of Sight.
              The Passes range from 30-365 days as well a prototype pass.
              </p>
          </div>
         
          <div className="row">
            <div className="col-sm-12 col-md-6 col-lg-6">
              <GamingZoneItem playerImg={card1} PlayerName="Buy with bnb" />
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <GamingZoneItem playerImg={card2} PlayerName="Buy with btc" />
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <GamingZoneItem playerImg={card3} PlayerName="Buy with bnb" />
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <GamingZoneItem playerImg={card4} PlayerName="Buy with btc" />
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <GamingZoneItem playerImg={card5} PlayerName="Buy with bnb" />
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <GamingZoneItem playerImg={card6} PlayerName="Buy with btc" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GamingZone;
