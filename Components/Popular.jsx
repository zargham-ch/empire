import ItemSlider from "./ItemSlider";
import PopulerItem from "./populerItem";

const a1 = "./assets/populerText.png";
const a2 = "./assets/populerTextV2.png";

const text1 = "./assets/MintingOneText.jpeg"
const text2 = "./assets/MintingTwoText.jpeg"
const text3 = "./assets/MintingThreeText.jpeg"

const Popular = () => {
  return (
    <div className="PopularWrapper">
      <ItemSlider />
      <div className="container">
         <div className="text-center w-100">
           <img src={a1} alt="popularText" className="popularText"/>
         </div>
      </div>
      <div className="container-fluid">
         <img src={a2} className="w-100 lg-view-now" />
         <div className="row sm-view-now no-gutters mx-0">
           <div className="col-sm-12 col-md-12 col-lg-4 mb-3">
             <img src={text1} className="img-fluid"/>
           </div>
            <div className="col-sm-12 col-md-12 col-lg-4 mb-3">
              <img src={text2} className="img-fluid"/>
            </div>
            <div className="col-sm-12 col-md-12 col-lg-4">
               <img src={text3} className="img-fluid"/>
            </div>                   
         </div>
      </div>
    </div>
  );
};

export default Popular;
