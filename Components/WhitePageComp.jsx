import React from "react";
const articleImage = "./assets/artImage.png";
const p1 = "./assets/c1.png";
const p2 = "./assets/c2.png";
const p3 = "./assets/c3.png";

const WhitePageComp = () => {
  return (
    <div className="container">
      <div className="pageWrapper">
        {/* <img src={articleImage} className="img-fluid mt-3 mb-5" /> */}
        <div className="headWrapper">
          <h6 className="heading">Introduction</h6>
          {/* <p className="para">Empire Of Sight</p> */}
        </div>
        <p className="paraDecText">
          I will be as blunt as possible for everyone reading this, we are not a pyramid scheme P2E game you see online where they only have a feature to mint-play-earn, mint-play-earn until the game dies. This is an actual game so do not expect the same type of fake whitepaper you see there promising you crazy things about the token and ROI in an attempt to make you think you will get rich instantly. We are here to deliver an actual game with a real stable gamelike economy based on true gamer values related to items, gold coins, a virtual token, quests, dungeons, guilds, bosses, but with a twist that includes a blockchain token and a virtual token. You would not see some fancy Pie Chart with everything split up into sections that mean absolutely nothing other than a false sense of security and you would not hear us telling you there is an insane ROI either. We will be direct and honest about the entire process to give you the choice of how you want to spend your time, money, and energy.
        </p>

        <h6 className="headingMain">Most questions you will have can be quickly answered by viewing the FAQS section on the website but we will cover some things here.</h6>

        <div className="headWrapper">
          <h6 className="heading">#1. How the game development team sustains revenue.</h6>
        </div>
        <p className="paraDecText">
          It is pretty simple, we will offer Access Passes that will allow us to generate the revenue needed to manage all marketing, servers, staff salary, and game development costs. In the future, we may develop a way to give unique NFT heroes directly for minting but for now, an Access Pass is all you will need to enter the game and play.
        </p>
        <div className="headWrapper">
          <h6 className="heading">#2 How you can profit from playing the gam</h6>
        </div>
        <p className="paraDecText">
          Well if you have ever played any MMORPG you know there is a large amount of the player base who buy gold, items, services, and many other assets in the game. In the Empire of Sight, it is no different! You will be able to farm gold, items, NFT, Virtual Tokens, blockchain tokens but here we endorse RWT we want you to look at everything you do as potential profit because you can sell whatever you want to other players independently in-game or VIA our marketplace. 
        </p>
        <div className="headWrapper">
          <h6 className="heading">#3 How Virtual Tokens and Blockchain Tokens will be awarded.</h6>
        </div>
        <p className="paraDecText">
          There will be a multitude of ways to obtain these currencies in the game, the primary way that you will be able to obtain them is through daily quests and bosses! For instance, in each major city, dungeon or area there will be Daily Quests that will reward them. Depending on your level, gear, unlocked areas, ability, and skill level in the game you will be able to earn more of them. The way it becomes a daily play to earn is through all activities in the game but for those who just want to hop on and do some quests to earn there will be daily play to earn quests that will allow this in a quick efficient manner, but only those who put the effort in will get to the areas of the game where the real potential is.
        </p>
        <div className="headWrapper">
          <h6 className="heading">#4 How the tokens have value.</h6>
        </div>
        <p className="paraDecText">
        n the game, there will be endless amounts of features that will require both tokens to, such as crafting, unlocking unique NFTs, cosmetics, consumables in the game, accessing special areas, bosses, dungeons, and more. You can charge a fee to enter your guild or pay others to join theirs. You can sell leveling services in-game or even buy speed runs while trying to get your most desired item, NFT, or level. This is an endless world of opportunity, fun, friendship, adventure, and loot.
        </p>
        <div className="headWrapper">
          <h6 className="heading">#5 How NFTs are obtained.</h6>
        </div>
        <p className="paraDecText">
        unting for NFTs can be fun, exciting, and sometimes even painful. Throughout your gameplay you will be able to collect materials to craft your own NFTs, you can purchase them from vendors for tokens, gold coins, or other various methods. They will be awarded through quests, completing a dungeon, working your way through a maze, killing a boss, or even a rare chance coming from a random monster kill! The game will be filled with wonderful exciting loot that you will want to record so you do not forget where it came from.
        </p>
        <div className="headWrapper">
          <h6 className="heading">#6 What blockchain we use.</h6>
        </div>
        <p className="paraDecText">
        We had to keep in mind a few aspects to make this game enjoyable & affordable while reaching out to as many as people we can. These aspects were low gas fees, transaction speed, credibility, and popularity. Thats why we chose the Binance Smart Chain.
        </p>
        <div className="headWrapper">
          <h6 className="heading">#7 Why we decided to use the ERC-1155 Multi Token Standard for items and access passes</h6>
        </div>
        <p className="paraDecText">
        As we know MMORPGs can have hundreds if not thousands of item types. Somehow we had to put our in-game items to the blockchain in the most efficient way. If we chose the well-known ERC-721 standard which the majority of NFTs are based on, we would have to deploy a separate smart contract for each item/token type. The ERC-1155 standard is flexible, it lets us use even thousands of token types in a single smart contract. That sounds exactly what we need, right?
        </p>
        <div className="headWrapper">
          <h6 className="heading">#8 How we structure our currency, items, and access passes on the blockchain.</h6>
        </div>
        <p className="paraDecText">
        Although the ERC-1155 standard has the mentioned flexibility to create thousands of token types in a single smart contract, we decided to go on the golden way and create at least a few categories, so our structure becomes more transparent for our players. Except for our currency which is an ERC-20 standard token, each item is based on the ERC-1155 standard. We defined the following categories: Access Passes, Weapons, Accessories, Potions, Armour Sets. Each category has a dedicated ERC-1155 smart contract and within a category, we have different item types. For example, we have the Weapons category, we have a separate Weapons smart contract, and within this category and smart contract, we have the following item/token types: Bow, Sword, Dagger, 2H Sword, Spear, Scythe, and so on... These types are separated by IDs and the trick is that more players can own from the same ID. The token types have limit caps, so an item can be owned only by a certain amount of players. Some of the items have many replicates, some have only a few or even only one. We truly wish you to be the one who will own some of the rarest items but you will have to play hard.
        </p>
      </div>
      {/* <div className="blogArticleWrapper">
        <div className="TempContainer">
          <div className="row">
            <div className="col-sm-12 col-md-6 col-lg-4">
              <div className="cardItem">
                <div className="cardHead">
                  <img src={p1} className="img-fluid img" />
                </div>
                <div className="cardBody">
                  <h6 className="head">long established</h6>
                  <p className="dec">
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page when looking at
                    its layout. The point of using Lorem Ipsum is that....
                  </p>
                  <span className="readDetail">
                    <span>May 20th 2020</span>
                    <span>Read more</span>
                  </span>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-4">
              <div className="cardItem">
                <div className="cardHead">
                  <img src={p2} className="img-fluid img" />
                </div>
                <div className="cardBody">
                  <h6 className="head">long established</h6>
                  <p className="dec">
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page when looking at
                    its layout. The point of using Lorem Ipsum is that....
                  </p>
                  <span className="readDetail">
                    <span>May 20th 2020</span>
                    <span>Read more</span>
                  </span>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-4">
              <div className="cardItem">
                <div className="cardHead">
                  <img src={p3} className="img-fluid img" />
                </div>
                <div className="cardBody">
                  <h6 className="head">long established</h6>
                  <p className="dec">
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page when looking at
                    its layout. The point of using Lorem Ipsum is that....
                  </p>
                  <span className="readDetail">
                    <span>May 20th 2020</span>
                    <span>Read more</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </div>
  );
};

export default WhitePageComp;
