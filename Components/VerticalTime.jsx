import React from "react";

const VerticalTime = () => {
  return (
    <div className="timelineWrapper">
      <div className="timeline">
        <ul className="mainTimeUl">
          <li className="mainTimeLi">
            <div className="content">
              <h6 className="headingMain timeLineHead">
                 <span>Phase 1</span>
              </h6>
              <ul className="timeLineList">
                <li className="listItem">Website Designed/Website Developed</li>
                <li className="listItem">Prototype Designed</li>
                <li className="listItem">Hand drawn artwork for site</li>
                <li className="listItem">NFT metrics for gameplay</li>
                <li className="listItem">5 layer economy designed</li>
                <li className="listItem">Open Discord to public</li>
              </ul>
            </div>
          </li>

          <li className="mainTimeLi">
            <div className="content">
              <h6 className="headingMain timeLineHead">
                <span>Phase 2</span>
              </h6>
              <ul className="timeLineList">
                <li className="listItem">Game Animations</li>
                <li className="listItem">NFT contracts</li>
                <li className="listItem">Funds added to NFT balance</li>
                <li className="listItem">Blockchain token on an exchange</li>
                <li className="listItem">Loot pool filled</li>
                <li className="listItem">Beta testing/ Bug fixes</li>
                <li className="listItem">Main Game production</li>
              </ul>
            </div>
          </li>

          <li className="mainTimeLi">
            <div className="content">
              <h6 className="headingMain timeLineHead">
                <span>Phase 3</span>
              </h6>
              <ul className="timeLineList">
                <li className="listItem">Marketing Phase 1</li>
                <li className="listItem">Prototype launch</li>
                <li className="listItem">Access Pass airdops</li>
                <li className="listItem">Guild Partnerships</li>
                <li className="listItem">Lore/Questline competitions</li>
                <li className="listItem">Hire additional map designers</li>
                <li className="listItem">Integrate new content</li>
              </ul>
            </div>
          </li>

          <li className="mainTimeLi">
            <div className="content">
              <h6 className="headingMain timeLineHead">
                <span>Phase 4</span>
              </h6>
              <p className="loremText">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the ind standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book. It has survived
                not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged.{" "}
              </p>
              <ul className="timeLineList mt-negative">
                <li className="listItem">Integrate new content</li>
                <li className="listItem">Prepare NFT contracts for maingame</li>
                <li className="listItem">Launch Maingame</li>
                <li className="listItem">Makertplace</li>
                <li className="listItem">Marketing Phase 2</li>
                <li className="listItem">Development Team for 3D MMORPG</li>
                <li className="listItem">Additional marketing and work with guilds</li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default VerticalTime;
