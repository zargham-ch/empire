import FutureRoadMap from "./FutureRoadMap";
import PopulerItem from "./populerItem";

const gameGirl = "./assets/mintingText.png";

const text = "./assets/MintingFourText.jpeg"
const MintingGame = () => {
  return (
    <div className="MintingGame">
      <div className="container-fluid">
        <img src={gameGirl} className="w-100 lg-view-now"/>
        <div className="row sm-view-now no-gutters mx-0">
          <div className="col-12">
             <img src={text} className="img-fluid"/>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MintingGame;
