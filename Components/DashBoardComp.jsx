import React from "react";
import DashBoardPlayerCard from "./DashBoardPlayerCard";
;

const DashBoardComp = () => {
  return (
    <div className="GamingZonePlay formPageWrapper dashboardWrapper abTwo">
      <div className="container">
        <h1 className="headingMain pt-10">
          <span>Dashbaord</span>
        </h1>
        <p className="dec mt-3">
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
          dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident.
        </p>
        <div className="rowWrapper pt-10 pb-10">
          <DashBoardPlayerCard  heading="Prototype Access" />
        </div>
        <div className="rowWrapper smallCardWrapper">
          <DashBoardPlayerCard classVersion="small-version" heading="Item Name here"/>
          <DashBoardPlayerCard classVersion="small-version" heading="Item Name here"/>
        </div>
      </div>
    </div>
  );
};

export default DashBoardComp;
