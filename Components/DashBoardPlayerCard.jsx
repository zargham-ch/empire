import React from 'react'
const playerImg = "./assets/playerOne.png";

const DashBoardPlayerCard = (props) => {
  return (
    <div className={`row ${props.classVersion}`}>
            <div className={` ${props.classVersion ? "col-sm-12 col-md-3 col-lg-3" : "col-sm-12 col-md-6 col-lg-6"}`}>
              <div className="playerWrapper">
                <img src={playerImg} alt="imagename" />
              </div>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <h1 className="headingMain pt-5 innerHead">
                <span>William Smith</span>
              </h1>
              <h6 className="sub-head">{props.heading}</h6>
              <p className="num-sub">9999 xp</p>
              <p className="num-sub ex-mb">9999 vt</p>
              <a href="#" className="btnGetStarted Withdraw mt-4">
                Withdraw (btn)
              </a>
            </div>
    </div>
  )
}

export default DashBoardPlayerCard