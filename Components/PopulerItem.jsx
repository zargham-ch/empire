import React from 'react'

const PopulerItem = (props) => {
  return (
             <div className="popularItemApp">
               <div className="headerTicket">
                  <h6 className="heading">
                  {props.heading}
                 </h6>
               </div>
               <p className="para">
                 {props.desc}
               </p>
             </div>
  )
}

export default PopulerItem
