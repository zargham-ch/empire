const facebook = "./assets/f1.png";
const discord = "./assets/discord.png";
const twitter = "./assets/f2.png";
const youtube = "./assets/f4.png";
const s1 = "./assets/sl1.png";
const s2 = "./assets/sl2.png";
const s3 = "./assets/sl3.png";
const s4 = "./assets/sl4.png";

const CopyRightFooter = () => {
  return (
    <div className="copyRightWrapper">
      <div className="left-policy">
        <a href="#" className="link">
          Terms & Conditions
        </a>
        <a href="#" className="link">
          Privacy Policy
        </a>
      </div>
      <div className="right-policy">
        <a href="#" className="link tw">
          <img src={s1} alt="facebook"  />
        </a>
        <a href="#" className="link tw">
          <img src={s2} alt="facebook" className="" />
        </a>
        <a href="#" className="link tw">
          <img src={s3} alt="twitter" />
        </a>
        <a href="#" className="link tw">
          <img src={s4} alt="insta" />
        </a>
         <a href="#" className="link">
          <img src={discord} alt="facebook" className="" />
        </a>
        <a href="#" className="link">
          <img src={facebook} alt="facebook" className="fb" />
        </a>
        <a href="#" className="link">
          <img src={twitter} alt="twitter" />
        </a>
        <a href="#" className="link">
          <img src={youtube} alt="insta" />
        </a>
      </div>
    </div>
  );
};

export default CopyRightFooter;
